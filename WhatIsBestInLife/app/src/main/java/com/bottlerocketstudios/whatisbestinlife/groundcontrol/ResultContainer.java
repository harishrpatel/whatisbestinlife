package com.bottlerocketstudios.whatisbestinlife.groundcontrol;

/**
 */
public class ResultContainer {
    public static final int SUCCESS = 0;
    public static final int FAILED = 1;

    private int resultCode;
    private String resultJson;

    public int getResultCode() {
        return resultCode;
    }

    public String getResultJson() {
        return resultJson;
    }

    public void setResultJson(String jsonStr) {
        resultJson = jsonStr;
    }

    public void setResultCode(int resultCodeIn) {
        resultCode = resultCodeIn;
    }
}
