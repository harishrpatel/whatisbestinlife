package com.bottlerocketstudios.whatisbestinlife.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bottlerocketstudios.whatisbestinlife.R;

/**
 * What is Best In Life?
 * Show options to select from.
 */
public class HomeFragment extends Fragment {

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int resId = v.getId();
            switch(resId) {
                case R.id.btn_add_a_thing:
                    break;
                case R.id.btn_compare_things:
                    Intent intent = CompareThingsActivity.newIntent(getActivity());
                    startActivity(intent);
                    break;
                case R.id.btn_view_top_things:
                    break;
            }

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        Button button = (Button) view.findViewById(R.id.btn_add_a_thing);
        button.setOnClickListener(mOnClickListener);

        button = (Button) view.findViewById(R.id.btn_compare_things);
        button.setOnClickListener(mOnClickListener);

        button = (Button) view.findViewById(R.id.btn_view_top_things);
        button.setOnClickListener(mOnClickListener);

        return view;
    }

    public static Fragment newInstance() {
        return new HomeFragment();
    }
}
