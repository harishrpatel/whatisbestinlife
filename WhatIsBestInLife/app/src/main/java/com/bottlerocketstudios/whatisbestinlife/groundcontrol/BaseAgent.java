package com.bottlerocketstudios.whatisbestinlife.groundcontrol;

import android.content.Context;

import com.bottlerocketapps.groundcontrol.agent.AbstractAgent;

/**
 */
public abstract class BaseAgent extends AbstractAgent<ResultContainer,Float> {
    private final Context mContext;
    protected float mProgress;
    protected boolean mCancelled;

    public BaseAgent(Context context) {
        mContext = context.getApplicationContext();
    }

    @Override
    public abstract String getUniqueIdentifier();

    @Override
    public void cancel() {
        mCancelled = true;
    }

    @Override
    public void onProgressUpdateRequested() {
        notifyProgress();
    }

    protected void notifyProgress() {
        getAgentListener().onProgress(getUniqueIdentifier(), mProgress);
    }

    public abstract void run();

    public abstract void setResult(ResultContainer resultContainer);
}
