package com.bottlerocketstudios.whatisbestinlife.model;

/**
 */
public class CompareThings {

    private String name;
    private String urlString;
    private String imgUrlString;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgUrlString() {
        return imgUrlString;
    }

    public void setImgUrlString(String imgUrlString) {
        this.imgUrlString = imgUrlString;
    }

    public String getUrlString() {
        return urlString;
    }

    public void setUrlString(String urlString) {
        this.urlString = urlString;
    }

}
