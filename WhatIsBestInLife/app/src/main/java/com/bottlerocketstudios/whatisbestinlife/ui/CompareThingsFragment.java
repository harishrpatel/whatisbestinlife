package com.bottlerocketstudios.whatisbestinlife.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bottlerocketapps.brfoundation.http.BRHttpProgress;
import com.bottlerocketapps.brfoundation.http.BRHttpResponse;
import com.bottlerocketapps.brfoundation.http.HttpClientService;
import com.bottlerocketapps.brfoundation.tools.Log;
import com.bottlerocketapps.groundcontrol.AgentExecutor;
import com.bottlerocketapps.groundcontrol.listener.AgentListener;
import com.bottlerocketapps.groundcontrol.policy.AgentPolicy;
import com.bottlerocketapps.groundcontrol.policy.StandardAgentPolicyBuilder;
import com.bottlerocketapps.groundcontrol.tether.AgentTether;
import com.bottlerocketapps.groundcontrol.tether.UIOneTimeAgentHelper;
import com.bottlerocketstudios.whatisbestinlife.R;
import com.bottlerocketstudios.whatisbestinlife.groundcontrol.CompareThingsAgent;
import com.bottlerocketstudios.whatisbestinlife.groundcontrol.ResultContainer;

/**
 * What is Best In Life?
 * Compare Things.
 */
public class CompareThingsFragment extends BaseFragment {

    private static final String TAG = CompareThingsFragment.class.getSimpleName();
    private TextView mTextView1;
    private TextView mTextView2;

    private UIOneTimeAgentHelper mCompareThingsAgentHelper = new UIOneTimeAgentHelper("CompareThingsAgent");

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int resId = v.getId();
            switch(resId) {
                case R.id.btnIsBetter1:  // continue
                case R.id.imageView1:
                    break;
                case R.id.btnReportProblem1:
                    break;
                case R.id.btnIsBetter2:  // continue
                case R.id.imageView2:
                    break;
                case R.id.btnReportProblem2:
                    break;
            }

        }
    };

    private AgentListener<ResultContainer,Float> mAgentListener = new AgentListener<ResultContainer, Float>() {
        @Override
        public void onCompletion(String agentIdentifier, ResultContainer result) {
            Log.d(TAG, "[onCompletion] resultCode = " + result.getResultCode());
            mCompareThingsAgentHelper.onAgentCompletion();

            //TODO save data
            if (result.getResultCode() == ResultContainer.SUCCESS) {
                String jsonStr = result.getResultJson();
            }
        }

        @Override
        public void onProgress(String agentIdentifier, Float progress) {
            //Log.d(TAG, "[onProgress]");
        }
    };

    private HttpClientService.HttpClientListener mHttpClientListener = new HttpClientService.HttpClientListener() {

        @Override
        public void onHttpClientResult(int i, boolean b, BRHttpResponse brHttpResponse) {
            String jsonStr = "";
            ResultContainer resultContainer = new ResultContainer();

            if (brHttpResponse.isHttpStatusOk()) {
                jsonStr = brHttpResponse.getResponseData();
                Log.d(TAG, "comparison json = " + jsonStr);
                resultContainer.setResultCode(ResultContainer.SUCCESS);
            } else {
                Log.e(TAG, "comparison result: error status: " + brHttpResponse.getErrorData());
                resultContainer.setResultCode(ResultContainer.FAILED);
            }

            resultContainer.setResultJson(jsonStr);
            Log.d(TAG, "resultContainer returned");

            mCompareThingsAgent.setResult(resultContainer);
        }

        @Override
        public void onHttpClientProgress(int i, BRHttpProgress brHttpProgress) {

        }
    };
    private CompareThingsAgent mCompareThingsAgent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_compare_things, container, false);

        // Object 1
        Button button = (Button) view.findViewById(R.id.btnIsBetter1);
        button.setOnClickListener(mOnClickListener);

        button = (Button) view.findViewById(R.id.btnReportProblem1);
        button.setOnClickListener(mOnClickListener);

        ImageView imageView1 = (ImageView) view.findViewById(R.id.imageView1);
        imageView1.setOnClickListener(mOnClickListener);

        mTextView1 = (TextView) view.findViewById(R.id.titleText1);

        // Object 2
        button = (Button) view.findViewById(R.id.btnIsBetter2);
        button.setOnClickListener(mOnClickListener);

        button = (Button) view.findViewById(R.id.btnReportProblem2);
        button.setOnClickListener(mOnClickListener);

        ImageView imageView2 = (ImageView) view.findViewById(R.id.imageView2);
        imageView2.setOnClickListener(mOnClickListener);

        mTextView2 = (TextView) view.findViewById(R.id.titleText2);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mCompareThingsAgentHelper.onSaveInstanceState(outState);
    }

    public static Fragment newInstance() {
        return new CompareThingsFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        startCompareThingsAgent();
    }

    private void registerNewTether(AgentTether agentTether) {
        mUiAgentTetherCollection.addTether(agentTether);
    }

    private void startCompareThingsAgent() {
        mCompareThingsAgent = new CompareThingsAgent(getActivity(), mHttpClientListener);
        AgentPolicy bypassCachePolicy = (new StandardAgentPolicyBuilder()).setBypassCache(false).build();

        registerNewTether(AgentExecutor.getDefault().runAgent(mCompareThingsAgent, bypassCachePolicy, mAgentListener));
    }
}
