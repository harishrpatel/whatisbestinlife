package com.bottlerocketstudios.whatisbestinlife.groundcontrol;

import android.content.Context;
import android.os.Looper;

import com.bottlerocketapps.brfoundation.http.BRHttpRequest;
import com.bottlerocketapps.brfoundation.http.HttpClientService;
import com.bottlerocketapps.brfoundation.tools.Log;
import com.bottlerocketstudios.whatisbestinlife.api.CompareThingsRequest;
import com.bottlerocketstudios.whatisbestinlife.model.CompareThings;

import java.util.List;

/**
 * Ground Control Agent to fetch Comparisons.
 */
public class CompareThingsAgent extends BaseAgent {

    private static final int NUM_COMPARISONS = 1;
    private static final String TAG = CompareThingsAgent.class.getSimpleName();
    private Context mContext;
    private List<CompareThings> mList;
    private HttpClientService.HttpClientListener mHttpClientListener;

    public CompareThingsAgent(Context context, HttpClientService.HttpClientListener httpClientListener) {
        super(context);
        mContext = context;
        mHttpClientListener = httpClientListener;
    }

    @Override
    public String getUniqueIdentifier() {
        return CompareThingsAgent.class.getCanonicalName();
    }

    @Override
    public void run() {
        boolean success = false;

        performAction();

        while (!mCancelled) {
            //Do some time consuming iterative work then notify 50% complete.
            mProgress = 0.5f;
            notifyProgress();
        }

    }

    private void performAction() {
        Looper.prepare();
        BRHttpRequest request = new CompareThingsRequest(mContext, NUM_COMPARISONS);

        HttpClientService.performHttpRequest(mContext, mHttpClientListener, request);
    }

    @Override
    public void setResult(ResultContainer resultContainer) {
        // notify agent
        getAgentListener().onCompletion(getUniqueIdentifier(), resultContainer);

    }
}
