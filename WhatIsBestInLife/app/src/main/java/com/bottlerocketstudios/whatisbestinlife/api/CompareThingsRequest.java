package com.bottlerocketstudios.whatisbestinlife.api;

import android.content.Context;

import com.bottlerocketapps.brfoundation.http.BRHttpRequest;

/**
 */
public class CompareThingsRequest extends BRHttpRequest {

    private static final String BASE_URL = "http://bestinlife.dev.bottlerocketservices.com/compare.php";
    private static final String QUERY_PARAM_UID = "UID";
    private static final String QUERY_PARAM_COUNT = "count";

    /** Acts as a session ID */
    private String getUniqueID() {
//        TelephonyManager mgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
//        return mgr.getDeviceId();
        return "1234567890";
    }
    public CompareThingsRequest(Context context, int count) {
        super(BASE_URL);

        if (count > 0) {
            putQueryParameter(QUERY_PARAM_COUNT, Integer.toString(count));
        }
        putQueryParameter(QUERY_PARAM_UID, getUniqueID());
    }
    @Override
    public String getUrl() {
        return BASE_URL;
    }
}
