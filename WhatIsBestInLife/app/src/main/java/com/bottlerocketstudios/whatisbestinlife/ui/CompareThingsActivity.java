package com.bottlerocketstudios.whatisbestinlife.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.bottlerocketstudios.whatisbestinlife.R;

/**
 * What is Best In Life?
 */
public class CompareThingsActivity extends FragmentActivity {

    private static final String COMPARE_THINGS_FRAG = "compare_things_frag";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_single_fragment);
        if (savedInstanceState == null) {
            Fragment frag = CompareThingsFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, frag, COMPARE_THINGS_FRAG)
                    .commit();
        }
    }

    public static Intent newIntent(Context context) {
        return new Intent(context, CompareThingsActivity.class);
    }
}
