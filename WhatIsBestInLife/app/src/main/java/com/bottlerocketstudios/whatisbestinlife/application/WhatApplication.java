package com.bottlerocketstudios.whatisbestinlife.application;

import android.app.Application;

import com.bottlerocketapps.brfoundation.application.BRApplicationConfig;
import com.bottlerocketapps.brfoundation.application.BRBaseApplication;

/**
 */
public class WhatApplication extends BRBaseApplication {
    @Override
    public BRApplicationConfig getAppConfig(BRBaseApplication brBaseApplication) {
        return new WhatAppConfig();
    }
}
