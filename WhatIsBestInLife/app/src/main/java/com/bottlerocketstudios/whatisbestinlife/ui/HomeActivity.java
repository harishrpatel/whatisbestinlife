package com.bottlerocketstudios.whatisbestinlife.ui;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.bottlerocketstudios.whatisbestinlife.R;

/**
 * What is Best In Life?
 */
public class HomeActivity extends FragmentActivity {

    private static final String HOME_FRAG = "home_frag";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_single_fragment);
        if (savedInstanceState == null) {
            Fragment frag = HomeFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, frag, HOME_FRAG)
                    .commit();
        }
    }

}
