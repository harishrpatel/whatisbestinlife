package com.bottlerocketstudios.whatisbestinlife.application;

import android.content.Context;

import com.bottlerocketapps.brfoundation.application.BRApplicationConfig;

/**
 */
public class WhatAppConfig extends BRApplicationConfig {
    @Override
    public boolean useBRImageManager() {
        return true;
    }

    @Override
    public boolean useBRAnalyticsManager() {
        return false;
    }

    @Override
    public int getMemoryBitmapCacheSizeMb(Context context) {
        return 2;
    }
}
