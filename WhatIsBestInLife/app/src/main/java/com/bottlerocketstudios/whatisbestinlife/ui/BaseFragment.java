package com.bottlerocketstudios.whatisbestinlife.ui;

import android.support.v4.app.Fragment;

import com.bottlerocketapps.groundcontrol.tether.UIAgentTetherCollection;

/**
 */
public class BaseFragment extends Fragment {
    protected UIAgentTetherCollection mUiAgentTetherCollection = new UIAgentTetherCollection();

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUiAgentTetherCollection.destroy();
    }

}
